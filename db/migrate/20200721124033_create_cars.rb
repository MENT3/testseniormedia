class CreateCars < ActiveRecord::Migration[6.0]
  def change
    create_table :cars do |t|
      t.string :brand
      t.string :model
      t.integer :mileage
      t.date :year

      t.timestamps
    end
  end
end
