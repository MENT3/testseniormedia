# Test technique Senior Media

## Installation

#### Prérequis:
Avoir installé [Ruby on Rails](http://installrails.com/)

#### Installation:

```bash
git clone https://gitlab.com/MENT3/testseniormedia.git
cd testseniormedia

# Installation des dépendances 
bundle install && yarn

# Création de la BDD
rails db:migrate
```

## Lancement

```bash
rails s
```

## Test
Lancement des tests utilisateurs

```bash
rails t test/models/user_test.rb
```