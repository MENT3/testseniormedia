class UsersController < ApplicationController
  before_action :set_user, only: [:show]
  before_action :authenticate_user!
  
  def show
  end

  private
    def set_user
      @user = User.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      flash[:notice] = "Aucun utilisateur disponible à cette adresse"
      redirect_to cars_path
    end

    def user_params
      params.require(:user).permit(:email, :password)
    end
end
