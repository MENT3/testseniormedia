json.extract! car, :id, :brand, :model, :mileage, :year, :created_at, :updated_at
json.url car_url(car, format: :json)
