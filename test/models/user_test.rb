require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'valid user' do
  	user = User.new(email: 'test@test.com', password: 'azertyuiop')
  	assert user.valid?
  end

  test 'invalid without password' do
  	user = User.new(email: 'test@test.com')
  	refute user.valid?, 'user is valid without password'
  end

  test 'invalid without email' do
  	user = User.new(password: 'azertyuiop')
  	refute user.valid?, 'user is valid without email'
  end
end