Rails.application.routes.draw do
  # Devises
  devise_for :users
	# Resources
  resources :users
  resources :cars, :path => '/'
end
